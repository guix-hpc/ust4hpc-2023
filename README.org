#+TITLE: Formation Guix pour UST4HPC 2023
#+AUTHOR: Ludovic Courtès
#+DATE: juin 2023

Vous trouverez ici le tutoriel Guix réalisé pour l’action nationale de
formation (ANF) [[https://calcul.math.cnrs.fr/2023-06-anf-ust4hpc.html][« User Tools for HPC » (UST4HPC)]] réalisée à Aussois en
juin 2023.

Diffusé sous [[https://creativecommons.org/licenses/by-sa/4.0/deed.fr][CC-BY-SA 4.0]].
